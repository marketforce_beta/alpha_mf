package Auth

import (
	"log"
	"strings"

	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	utils "gitlab.com/victorrb1015/alpha_mf/App/Utils"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
)

func Login(c *fiber.Ctx) error {
	type UserForm struct {
		Username string `json:"username" validate:"required"`
		Password string `json:"password" validate:"required"`
	}
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.SUser
	var company model.Company
	var userForm UserForm
	err = c.BodyParser(&userForm)
	if strings.ContainsAny(userForm.Username, "_") {
		// es
		var mortal model.User
		split := strings.Split(userForm.Username, "_")
		db.Find(&company, "abbreviation = ?", split[0])
		utils.ConnectClient(company.CompanyDb)
		dbm := database.DbC
		sqlDB2, err2 := db.DB()
		if err != nil {
			log.Fatalln(err2)
		}
		defer sqlDB2.Close()
		dbm.Find(&mortal, "username = ?", userForm.Username)
		if mortal.ID == 0 {
			// Client login
			return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
		}
		if !utils.CheckPasswordHash(userForm.Password, mortal.Password) {
			return c.Status(401).JSON(fiber.Map{"status": "error", "message": "your credentials are wrong", "data": nil})
		}
		// Generate encoded token and send it as response.
		token, err := utils.GenerateNewAccessToken(mortal.Username, mortal.Email)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": true,
				"msg":   err.Error(),
			})
		}
		return c.JSON(fiber.Map{
			"user_id":           mortal.ID,
			"name":              mortal.Name,
			"email":             mortal.Email,
			"last_name":         mortal.LastName,
			"super_user":        "No",
			"profile_id":        mortal.ProfileId,
			"profile":           "Mortal Profile",
			"token":             token,
			"company_id":        company.ID,
			"company_logo":      company.Logo,
			"company_orders":    company.OrdersLogo,
			"company_full_name": company.FullName,
			"company_database":  company.CompanyDb,
			"company_dir":       company.Address,
			"company_tel":       company.Phone})
	} else {
		if err != nil {
			return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
		}
		db.Find(&user, "username = ?", userForm.Username)
		if user.ID == 0 {
			// Client login
			return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
		}
		// If the password is incorrect, send an error
		if !utils.CheckPasswordHash(userForm.Password, user.Password) {
			return c.Status(401).JSON(fiber.Map{"status": "error", "message": "your credentials are wrong", "data": nil})
		}
		// Generate encoded token and send it as response.
		token, err := utils.GenerateNewAccessToken(user.Username, user.Email)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": true,
				"msg":   err.Error(),
			})
		}
		db.Find(&company, "id = ?", 4)
		return c.JSON(fiber.Map{
			"user_id":           user.ID,
			"name":              user.Name,
			"email":             user.Email,
			"last_name":         user.LastName,
			"super_user":        "Yes",
			"profile":           "Super Admin",
			"token":             token,
			"company_id":        company.ID,
			"company_logo":      company.Logo,
			"company_orders":    company.OrdersLogo,
			"company_full_name": company.FullName,
			"company_database":  company.CompanyDb,
			"company_dir":       company.Address,
			"company_tel":       company.Phone})
	}
}
