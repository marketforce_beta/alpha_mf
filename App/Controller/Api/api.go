package ApiController

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
)

func GetApis(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var apis []model.Api
	//search Apis
	db.Find(&apis)
	if len(apis) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Apis present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Apis Found", "data": apis})
}

func CreateApi(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	api := new(model.Api)
	err = c.BodyParser(api)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	err = db.Create(&api).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create api", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created Company", "data": api})
}

func GetApi(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var api model.Api
	//read apiId
	id := c.Params("apiId")
	//find Api
	db.Find(&api, "id = ?", id)
	if api.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No api present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Api Found", "data": api})
}

func UpdateApi(c *fiber.Ctx) error {
	type UpdateApi struct {
		Name        string `json:"name" validate:"required,lte=255"`
		Description string `json:"description" validate:"required"`
		Route       string `json:"ruta" validate:"required,lte=255"`
		Icon        string `json:"icon" validate:"required,lte=255"`
	}
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var api model.Api
	//read the param apiId
	id := c.Params("apiId")
	//find the api
	db.Find(&api, "id = ?", id)
	// If no such api present return an error
	if api.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No api present", "data": nil})
	}
	var updateApi UpdateApi
	err = c.BodyParser(&updateApi)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	api.Name = updateApi.Name
	api.Description = updateApi.Description
	api.Route = updateApi.Route
	api.Icon = updateApi.Icon

	db.Save(&api)
	return c.JSON(fiber.Map{"status": "success", "message": "Api Updated", "data": api})
}

func DeleteApi(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var api model.Api

	// Read the param noteId
	id := c.Params("apiId")

	// Find the note with the given Id
	db.Find(&api, "id = ?", id)

	// If no such note present return an error
	if api.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Api present", "data": nil})
	}

	// Delete the note and return error if encountered
	err = db.Delete(&api, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete Api", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Api"})
}
