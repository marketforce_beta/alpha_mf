package snotificationController

import (
	"database/sql"
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
)

func GetSNotifications(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var notification []model.SNotification
	// Read the param companyId
	id := c.Params("userId")
	db.Where("seen = false").Where("SUserId = ?", id).Find(&notification)
	if len(notification) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Notification present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Notification Found", "data": notification})
}

func CreateSNotifications(notification model.SNotification) {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	// Create the Notification and return error if encountered
	db.Create(&notification)
}

func SeenSNotification(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var notification model.SNotification
	id := c.Params("notificationId")
	db.Find(&notification, "id = ?", id)
	if notification.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No notification present", "data": nil})
	}
	t := sql.NullBool{}
	t.Valid = true
	t.Bool = true
	notification.Seen = t
	db.Save(&notification)
	return c.JSON(fiber.Map{"status": "success", "message": "Notification Updated"})
}

func SeenAllSNotification(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	id := c.Params("userId")
	err = db.Model(&model.Notification{}).Where("s_user_id = ?", id).Update("seen", "1").Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not update", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Notifications Updated"})
}
