package ApiCompanyController

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
)

func GetApiCompanies(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var apis []model.Api
	//read apiId
	id := c.Params("companyId")
	//search Apis
	db.Joins("companies").Joins("api_companies").Where("companies.id = ?", id).Find(&apis)
	if len(apis) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Apis present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Apis Found", "data": apis})
}

func CreateApiCompany(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	api := new(model.ApiCompany)
	err = c.BodyParser(api)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	err = db.Create(&api).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not Join the api", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created Company", "data": api})
}

func DeleteApi(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var api model.ApiCompany
	// Read the param noteId
	id := c.Params("apiId")
	id2 := c.Params("companyId")

	// Find the note with the given Id
	db.Where("api_id = ?", id).Where("company_id = ?", id2).Find(&api)

	// If no such note present return an error
	if api.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Api present", "data": nil})
	}

	// Delete the note and return error if encountered
	err = db.Delete(&api, "id = ?", api.ID).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete Api", "data": nil})
	}
	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Api"})
}
