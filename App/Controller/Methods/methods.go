package Methods

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
)

func GetMethods(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var methods []model.Methods
	//Search Methods
	db.Find(&methods)
	if len(methods) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Methods present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Apis Found", "data": methods})
}

func CreateMethod(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	method := new(model.Methods)
	err = c.BodyParser(method)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	err = db.Create(&method).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create method", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created Company", "data": method})
}

func GetMethod(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var method model.Methods
	//read methodId
	id := c.Params("methodId")
	//find Api
	db.Find(&method, "id = ?", id)
	if method.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No method present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Api Found", "data": method})
}

func UpdateMethod(c *fiber.Ctx) error {
	database.ConnectDB()
	type UpdateMethod struct {
		ApiId  uint   `json:"api_id" validate:"required"`
		Method string `json:"method" validate:"required,lte=255"`
		Path   string `json:"path" validate:"required,lte=255"`
		Type   string `json:"type" validate:"required,lte=255"`
	}
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var method model.Methods
	//read the param methodId
	id := c.Params("methodId")
	//find the api
	db.Find(&method, "id = ?", id)
	// If no such api present return an error
	if method.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No method present", "data": nil})
	}
	var updateMethod UpdateMethod
	err = c.BodyParser(&updateMethod)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	method.ApiId = updateMethod.ApiId
	method.Method = updateMethod.Method
	method.Type = updateMethod.Type
	method.Path = updateMethod.Path

	db.Save(&method)
	return c.JSON(fiber.Map{"status": "success", "message": "updateMethod Updated", "data": method})
}

func DeleteMethod(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var method model.Methods

	// Read the param methodId
	id := c.Params("methodId")

	// Find the note with the given Id
	db.Find(&method, "id = ?", id)

	// If no such note present return an error
	if method.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No method present", "data": nil})
	}

	// Delete the note and return error if encountered
	err = db.Delete(&method, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete Method", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Method"})
}
