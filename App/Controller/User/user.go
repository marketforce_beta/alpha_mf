package User

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	passwordvalidator "github.com/wagslane/go-password-validator"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	utils "gitlab.com/victorrb1015/alpha_mf/App/Utils"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
	"strconv"
)

var MIN_ENTROPY_BITS = config.Config("MIN_ENTROPY_BITS")

func GetUsers(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var users []model.User
	// find all users in the database
	db.Find(&users)
	// If no note is present return an error
	if len(users) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No users present", "data": nil})
	}
	// Else return notes
	return c.JSON(fiber.Map{"status": "success", "message": "Users Found", "data": users})
}

func GetCountUser(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user []model.User
	var count int64

	db.Find(&user).Count(&count)
	if len(user) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No super users present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Super Users Found", "data": count})
}

func CreateUser(c *fiber.Ctx) error {
	fmt.Println(string(c.Body()))
	data := c.Params("database")
	dbm := database.DB
	var company model.Company
	user := new(model.User)
	err := c.BodyParser(user)
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	mistake := dbm.Find(&company, "id = ?", user.CompanyId).Error
	if mistake != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": mistake})
	}

	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	user.Username = company.Abbreviation + "_" + user.Username
	if !utils.Valid(user.Email) {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email is not valid", "data": err})
	}
	intVar, _ := strconv.ParseFloat(MIN_ENTROPY_BITS, 64)
	err = passwordvalidator.Validate(user.Password, intVar)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your password is not secure", "data": err})
	}
	secret, _ := utils.HashPassword(user.Password)
	user.Password = secret
	err = db.Create(&user).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create user", "data": err})
	}
	// Return the created note
	return c.JSON(fiber.Map{"status": "success", "message": "Created user", "data": user})
}

func GetUser(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.User
	// Read the param noteId
	id := c.Params("userId")
	// Find the note with the given Id
	db.Find(&user, "id = ?", id)
	// If no such note present return an error
	if user.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user present", "data": nil})
	}
	// Return the note with the Id
	return c.JSON(fiber.Map{"status": "success", "message": "User Found", "data": user})
}

func UpdateUser(c *fiber.Ctx) error {
	type updateUser struct {
		CompanyId uint   `json:"company_id"`
		Name      string `json:"name"`
		LastName  string `json:"last_name"`
		Password  string `json:"password"`
		Email     string `json:"email"`
		Username  string `json:"username"`
		ProfileId uint   `json:"profile_id"`
	}
	// Read the params
	data := c.Params("database")
	id := c.Params("userId")
	dbm := database.DB
	var company model.Company
	var user model.User
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	// Find the note with the given Id
	db.Find(&user, "id = ?", id)
	mistake := dbm.Find(&company, "id = ?", user.CompanyId).Error
	if mistake != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": mistake})
	}
	// If no such note present return an error
	if user.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user present", "data": nil})
	}
	// Store the body containing the updated data and return error if encountered
	var updateUserData updateUser
	err = c.BodyParser(&updateUserData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	// validate Info
	if !utils.Valid(updateUserData.Email) {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email is not valid", "data": err})
	}
	intVar, _ := strconv.ParseFloat(MIN_ENTROPY_BITS, 64)
	err = passwordvalidator.Validate(updateUserData.Password, intVar)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your password is not secure", "data": err})
	}
	user.Username = company.Abbreviation + "_" + updateUserData.Username
	user.Name = updateUserData.Name
	user.LastName = updateUserData.LastName
	user.Password, _ = utils.HashPassword(updateUserData.Password)
	user.Email = updateUserData.Email
	user.ProfileId = updateUserData.ProfileId
	// Save the Changes
	db.Save(&user)
	// Return the updated note
	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": user})
}

func DeleteUser(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.User
	//Read Param userId
	id := c.Params("userId")
	//find the user
	db.Find(&user, "id = ?", id)
	//if no such user present an error
	if user.ID == 0 {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
	}
	//Delete the user
	err = db.Delete(&user, "id = ?", id).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Failed to delete User", "data": nil})
	}
	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted User"})
}
