package suserController

import (
	snotificationController "gitlab.com/victorrb1015/alpha_mf/App/Controller/SNotification"
	Utils "gitlab.com/victorrb1015/alpha_mf/App/Utils"
	"log"
	"strconv"

	"github.com/gofiber/fiber/v2"
	passwordvalidator "github.com/wagslane/go-password-validator"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
)

var MIN_ENTROPY_BITS = config.Config("MIN_ENTROPY_BITS")

func GetSUsers(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var suser []model.SUser
	// Busca todas las companias en la base de datos
	db.Find(&suser)

	if len(suser) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No super users present", "data": nil})
	}
	// Else return companies
	return c.JSON(fiber.Map{"status": "success", "message": "Super Users Found", "data": suser})
}

func GetCountSUser(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var suser []model.SUser
	var count int64

	db.Find(&suser).Count(&count)
	if len(suser) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No super users present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Super Users Found", "data": count})
}

func CreateSUser(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	suser := new(model.SUser)

	err = c.BodyParser(suser)

	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input ", "data": err})
	}

	if !Utils.Valid(suser.Email) {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email is not valid", "data": err})
	}
	intVar, _ := strconv.ParseFloat(MIN_ENTROPY_BITS, 64)
	err = passwordvalidator.Validate(suser.Password, intVar)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your password is not secure", "data": err})
	}

	secret, _ := Utils.HashPassword(suser.Password)
	suser.Password = secret
	err = db.Create(&suser).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create User", "data": err})
	}
	// create notification
	var notification model.SNotification
	notification.Icon = "user-plus"
	notification.Color = "green"
	notification.Title = "New User registered"
	notification.Description = "Welcome " + suser.Username + " " + suser.LastName
	notification.SUserId = suser.ID
	snotificationController.CreateSNotifications(notification)
	return c.JSON(fiber.Map{"status": "success", "message": "Created User", "data": suser})
}

func GetUser(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.SUser
	// Reed the param userId
	id := c.Params("userId")
	// find the user
	db.Find(&user, "id = ?", id)
	// if no such user present return an error
	if user.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
	}
	//return the user with ID
	return c.JSON(fiber.Map{"status": "success", "message": "User found", "data": user})
}

func UpdateUser(c *fiber.Ctx) error {
	type UpdateSUser struct {
		CompanyId uint   `json:"company_id" validate:"required"`
		Name      string `json:"name" validate:"required,lte=255"`
		LastName  string `json:"last_name"`
		Username  string `json:"username" validate:"required,lte=255"`
		Password  string `json:"password" validate:"required,lte=255"`
		Email     string `json:"email" validate:"required,lte=255"`
	}
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.SUser
	//read the param userId
	id := c.Params("userId")
	//find the user
	db.Find(&user, "id = ?", id)
	if 0 == user.ID {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
	}
	var updateUser UpdateSUser
	err = c.BodyParser(&updateUser)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	if !Utils.Valid(updateUser.Email) {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email is not valid", "data": err})
	}
	intVar, _ := strconv.ParseFloat(MIN_ENTROPY_BITS, 64)
	err = passwordvalidator.Validate(updateUser.Password, intVar)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your password is not secure", "data": err})
	}

	user.CompanyId = updateUser.CompanyId
	user.Name = updateUser.Name
	user.LastName = updateUser.LastName
	user.Username = updateUser.Username
	user.Password, _ = Utils.HashPassword(updateUser.Password)
	user.Email = updateUser.Email
	//save Changes
	db.Save(&user)
	return c.JSON(fiber.Map{"status": "success", "message": "User Update", "data": user})
}

func DeleteUser(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var user model.SUser
	//Read Param userId
	id := c.Params("userId")
	//find the user
	db.Find(&user, "id = ?", id)
	//if no such user present an error
	if user.ID == 0 {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "No User present", "data": nil})
	}
	//Delete the user
	err = db.Delete(&user, "id = ?", id).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Failed to delete User", "data": nil})
	}
	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted User"})
}
