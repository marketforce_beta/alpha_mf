package companyController

import (
	"github.com/gofiber/fiber/v2"
	passwordvalidator "github.com/wagslane/go-password-validator"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	"gitlab.com/victorrb1015/alpha_mf/App/Utils"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
	"strconv"
)

var MIN_ENTROPY_BITS = config.Config("MIN_ENTROPY_BITS")

func GetCompanies(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var companies []model.Company
	// Busca todas las companias en la base de datos
	db.Find(&companies)

	if len(companies) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Companies present", "data": nil})
	}
	// Else return companies
	return c.JSON(fiber.Map{"status": "success", "message": "Companies Found", "data": companies})
}

func CreateCompanies(c *fiber.Ctx) error {
	type CreateCompany struct {
		FullName     string `json:"full_name" validate:"required,lte=255"`
		Address      string `json:"address" validate:"required,lte=255"`
		Abbreviation string `json:"abbreviation" validate:"required,lte=255"`
		Logo         string `json:"logo"`
		OrdersLogo   string `json:"orders_logo"`
		Country      string `json:"country" validate:"required,lte=255"`
		City         string `json:"city" validate:"required,lte=255"`
		State        string `json:"state" validate:"required,lte=255"`
		ZipCode      string `json:"zip_code" validate:"required,lte=255"`
		EmailFrom    string `json:"email_from" validate:"required,lte=255"`
		Phone        string `json:"phone" validate:"required,lte=255"`
		Database     string `json:"database" validate:"required,lte=255"`
		WebSite      string `json:"website" validate:"required,lte=255"`
		Name         string `db:"name" json:"name" validate:"required,lte=255"`
		LastName     string `db:"last_name" json:"last_name"`
		Password     string `db:"password" json:"password" validate:"required,lte=255"`
		Username     string `gorm:"unique" db:"username" json:"username" validate:"required,lte=255"`
		Email        string `db:"email" json:"email" validate:"required,lte=255"`
		ProfileId    uint   `json:"profile_id"`
	}
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	company := new(model.Company)
	user := new(model.User)
	var create CreateCompany
	err = c.BodyParser(&create)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	// Create CompanyDb Company
	/*
		if !Utils.Valid(create.EmailFrom) {
			return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email From is not valid", "data": err})
		}
		if !Utils.Valid(create.Email) {
			return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your email From is not valid", "data": err})
		}*/
	intVar, _ := strconv.ParseFloat(MIN_ENTROPY_BITS, 64)
	err = passwordvalidator.Validate(create.Password, intVar)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Your password is not secure", "data": err})
	}
	secret, _ := Utils.HashPassword(create.Password)
	/*logo, err := c.FormFile("logo")
	OrdersLogo, err := c.FormFile("orders_logo")
	if err != nil {
		log.Println("image upload error --> ", err)
		return c.JSON(fiber.Map{"status": 409, "message": "Fail To upload Images", "data": err})
	}
	// generate new uuid for image name
	uniqueLogo := uuid.New()
	uniqueOrdersLogo := uuid.New()
	// remove "- from imageName"
	filename := strings.Replace(uniqueLogo.String(), "-", "", -1)
	fileOrdersName := strings.Replace(uniqueOrdersLogo.String(), "-", "", -1)
	// extract image extension from original file filename
	fileExt := strings.Split(logo.Filename, ".")[1]
	fileExt2 := strings.Split(OrdersLogo.Filename, ".")[1]
	// generate image from filename and extension
	image := fmt.Sprintf("%s.%s", filename, fileExt)
	imageOrder := fmt.Sprintf("%s.%s", fileOrdersName, fileExt2)
	// save image to ./images dir
	err = c.SaveFile(logo, fmt.Sprintf("./images/%s", image))
	err = c.SaveFile(OrdersLogo, fmt.Sprintf("./images/%s", imageOrder))
	if err != nil {
		log.Println("image save error --> ", err)
		return c.JSON(fiber.Map{"status": 409, "message": "Fail To upload Images", "data": err})
	}
	// generate image url to serve to client using CDN
	imageUrl := fmt.Sprintf("http://localhost:3000/images/%s", image)
	imageUrlOrder := fmt.Sprintf("http://localhost:3000/images/%s", imageOrder)*/
	err = db.Exec("CREATE DATABASE " + create.Database).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create database for company, review your input", "data": err})
	}
	// Migrate database
	database.ConnectdbClient(create.Database)
	Dbc := database.DbC
	sqlDB2, err2 := Dbc.DB()
	if err2 != nil {
		log.Fatalln(err2)
	}
	defer sqlDB2.Close()
	encryptedDb := Utils.Encrypt(create.Database)
	company.FullName = create.FullName
	company.Address = create.Address
	company.Abbreviation = create.Abbreviation
	company.Logo = create.Logo
	company.OrdersLogo = create.OrdersLogo
	company.Country = create.Country
	company.City = create.City
	company.State = create.State
	company.ZipCode = create.ZipCode
	company.EmailFrom = create.EmailFrom
	company.Phone = create.Phone
	company.CompanyDb = encryptedDb
	company.WebSite = create.WebSite
	// Create the Note and return error if encountered
	err = db.Create(&company).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create company", "data": err})
	}
	db.First(&company)
	user.CompanyId = company.ID
	user.Name = create.Name
	user.LastName = create.LastName

	user.Password = secret
	user.Username = company.Abbreviation + "_" + create.Username
	user.Email = create.Email
	user.ProfileId = 1
	err = Dbc.Debug().Model(&model.User{}).Create(&user).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create user", "data": err})
	}
	// Return the created note
	return c.JSON(fiber.Map{"status": "success", "message": "Created Company", "Company": company, "user": user})
}

func GetCompany(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var company model.Company

	// Read the param companyId
	id := c.Params("companyId")

	// Find the company with the given Id
	db.Find(&company, "id = ?", id)

	// If no such note present return an error
	if company.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No company present", "data": nil})
	}

	// Return the note with the Id
	return c.JSON(fiber.Map{"status": "success", "message": "Company Found", "data": company})
}

func UpdateCompany(c *fiber.Ctx) error {
	type UpdateCompany struct {
		FullName     string `json:"full_name" validate:"required,lte=255"`
		Address      string `json:"address" validate:"required,lte=255"`
		Abbreviation string `json:"abbreviation" validate:"required,lte=255"`
		Logo         string `json:"logo" validate:"required"`
		OrdersLogo   string `json:"orders_logo" validate:"required"`
		Country      string `json:"country" validate:"required,lte=255"`
		City         string `json:"city" validate:"required,lte=255"`
		State        string `json:"state" validate:"required,lte=255"`
		ZipCode      string `json:"zip_code" validate:"required,lte=255"`
		EmailFrom    string `json:"email_from" validate:"required,lte=255"`
		Phone        string `json:"phone" validate:"required,lte=255"`
		Database     string `json:"database" validate:"required,lte=255"`
		WebSite      string `json:"website" validate:"required,lte=255"`
	}
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var company model.Company

	// Read the param noteId
	id := c.Params("companyId")

	// Find the note with the given Id
	db.Find(&company, "id = ?", id)

	// If no such note present return an error
	if company.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No company present", "data": nil})
	}

	// Store the body containing the updated data and return error if encountered
	var updateCompanyData UpdateCompany
	err = c.BodyParser(&updateCompanyData)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	// Edit the note
	company.FullName = updateCompanyData.FullName
	company.Address = updateCompanyData.Address
	company.Abbreviation = updateCompanyData.Abbreviation
	company.Logo = updateCompanyData.Logo
	company.OrdersLogo = updateCompanyData.OrdersLogo
	company.Country = updateCompanyData.Country
	company.City = updateCompanyData.City
	company.State = updateCompanyData.State
	company.ZipCode = updateCompanyData.ZipCode
	company.EmailFrom = updateCompanyData.EmailFrom
	company.Phone = updateCompanyData.Phone
	company.WebSite = updateCompanyData.WebSite

	// Save the Changes
	db.Save(&company)

	// Return the updated note
	return c.JSON(fiber.Map{"status": "success", "message": "Company Updated", "data": company})
}

func DeleteCompany(c *fiber.Ctx) error {
	database.ConnectDB()
	db := database.DB
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var company model.Company

	// Read the param noteId
	id := c.Params("companyId")

	// Find the note with the given Id
	db.Find(&company, "id = ?", id)

	// If no such note present return an error
	if company.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Company present", "data": nil})
	}

	// Delete the note and return error if encountered
	err = db.Delete(&company, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete Company", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Company"})
}
