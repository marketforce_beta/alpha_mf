package Profile

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	utils "gitlab.com/victorrb1015/alpha_mf/App/Utils"
	database "gitlab.com/victorrb1015/alpha_mf/Database"
	"log"
)

func GetProfiles(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var profile []model.Profile
	db.Find(&profile)
	if len(profile) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Profiles present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Profiles Found", "data": profile})
}

func CreateProfile(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	dbm := database.DbC
	sqlDB, err := dbm.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	profile := new(model.Profile)
	err = c.BodyParser(profile)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	err = dbm.Create(&profile).Error
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Could not create profile", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created profile", "data": profile})
}

func GetProfile(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var profile model.Profile
	id := c.Params("profileId")
	db.Find(&profile, "id = ?", id)
	if profile.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Profile Found", "data": profile})
}

func UpdateProfile(c *fiber.Ctx) error {
	type UpdateProfile struct {
		Name        string `json:"name" validate:"required,lte=255"`
		Description string `json:"description" validate:"required,lte=255"`
	}
	data := c.Params("database")
	id := c.Params("profileId")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var profile model.Profile
	db.Find(&profile, "id = ?", id)
	if profile.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No profile present", "data": nil})
	}
	var update UpdateProfile
	err = c.BodyParser(update)
	if err != nil {
		return c.Status(409).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	profile.Name = update.Name
	profile.Description = update.Description
	db.Save(&profile)
	return c.JSON(fiber.Map{"status": "success", "message": "Company Updated", "data": profile})
}

func DeleteProfile(c *fiber.Ctx) error {
	data := c.Params("database")
	id := c.Params("profileId")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var profile model.Profile
	db.Find(&profile, "id = ?", id)
	if profile.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No profile present", "data": nil})
	}
	// Delete the note and return error if encountered
	err = db.Delete(&profile, "id = ?", id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete profile", "data": nil})
	}
	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted profile"})
}
