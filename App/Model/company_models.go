package model

import (
	"database/sql"
	"gorm.io/gorm"
)

type Company struct {
	gorm.Model
	FullName     string       `db:"full_name" json:"full_name" validate:"required,lte=255"`
	Address      string       `db:"address" json:"address" validate:"lte=255"`
	Abbreviation string       `db:"abbreviation" json:"abbreviation" validate:"required,lte=255"`
	Logo         string       `db:"logo" json:"logo" validate:"required"`
	OrdersLogo   string       `db:"orders_logo" json:"orders_logo" validate:"required"`
	Country      string       `db:"country" json:"country" validate:"required,lte=255"`
	City         string       `db:"city" json:"city" validate:"required,lte=255"`
	State        string       `db:"state" json:"state" validate:"required,lte=255"`
	ZipCode      string       `db:"zip_code" json:"zip_code" validate:"required,lte=255"`
	EmailFrom    string       `db:"email_from" json:"email_from" validate:"required,lte=255"`
	Phone        string       `db:"Phone" json:"phone" validate:"required,lte=255"`
	CompanyDb    string       `db:"company_db" json:"company_db" validate:"required,lte=255"`
	WebSite      string       `db:"website" json:"website" validate:"required,lte=255"`
	SUser        []SUser      `gorm:"foreignKey:CompanyId" validate:"required"`
	User         []User       `gorm:"foreignKey:CompanyId" validate:"required"`
	ApiCompany   []ApiCompany `gorm:"foreignKey:CompanyId" validate:"required"`
}

type SUser struct {
	gorm.Model
	CompanyId     uint            `json:"company_id" validate:"required"`
	Name          string          `db:"name" json:"name" validate:"required,lte=255"`
	LastName      string          `db:"last_name" json:"last_name"`
	Username      string          `gorm:"unique" db:"username" json:"username" validate:"required,lte=255"`
	Password      string          `db:"password" json:"password" validate:"required,lte=255"`
	Email         string          `db:"email" json:"email" validate:"required,lte=255"`
	SNotification []SNotification `gorm:"foreignKey:SUserId" validate:"required"`
}

type SNotification struct {
	gorm.Model
	SUserId     uint         `json:"s_user_id" validate:"required"`
	Title       string       `db:"title" json:"title" validate:"required,lte=255"`
	Description string       `db:"description" json:"description" validate:"required,lte=255"`
	Icon        string       `db:"icon" json:"icon" validate:"required,lte=255"`
	Color       string       `db:"color" json:"color" validate:"required,lte=255"`
	Seen        sql.NullBool `gorm:"default:false" db:"seen" json:"seen" validate:"required"`
}

type ApiCompany struct {
	gorm.Model
	ApiId     uint `json:"api_id" validate:"required"`
	CompanyId uint `json:"company_id" validate:"required"`
}

type Api struct {
	gorm.Model
	Name        string       `db:"name" json:"name" validate:"required,lte=255"`
	Description string       `db:"description" json:"description" validate:"required"`
	Route       string       `db:"ruta" json:"ruta" validate:"required,lte=255"`
	Icon        string       `db:"icon" json:"icon" validate:"required,lte=255"`
	ApiCompany  []ApiCompany `gorm:"foreignKey:ApiId"`
	Methods     []Methods    `gorm:"foreignKey:ApiId"`
}

type Methods struct {
	gorm.Model
	ApiId  uint   `json:"api_id" validate:"required"`
	Method string `db:"method" json:"method" validate:"required,lte=255"`
	Path   string `db:"path" json:"path" validate:"required,lte=255"`
	Type   string `db:"type" json:"type" validate:"required,lte=255"`
}
