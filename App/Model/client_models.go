package model

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	CompanyId    uint           `json:"company_id" validate:"required"`
	Name         string         `db:"name" json:"name" validate:"required,lte=255"`
	LastName     string         `db:"last_name" json:"last_name"`
	Password     string         `db:"password" json:"password" validate:"required,lte=255"`
	Username     string         `gorm:"unique" db:"username" json:"username" validate:"required,lte=255"`
	Email        string         `db:"email" json:"email" validate:"required,lte=255"`
	ProfileId    uint           `json:"profile_id" validate:"required"`
	Notification []Notification `gorm:"foreignkey:UserId" validate:"required"`
}

type Profile struct {
	gorm.Model
	Name        string   `db:"name" json:"name" validate:"required,lte=255"`
	Description string   `db:"description" json:"description" validate:"required,lte=255"`
	Action      []Action `gorm:"foreignKey:ProfileId" validate:"required"`
	User        []User   `gorm:"foreignkey:ProfileId" validate:"required"`
}

type Notification struct {
	gorm.Model
	UserId      uint   `json:"user_id" validate:"required"`
	Title       string `db:"title" json:"title" validate:"required,lte=255"`
	Description string `db:"description" json:"description" validate:"required,lte=255"`
	Icon        string `db:"icon" json:"icon" validate:"required,lte=255"`
	Color       string `db:"color" json:"color" validate:"required,lte=255"`
}

type Action struct {
	gorm.Model
	ProfileId uint `json:"profile_id" validate:"required"`
	MethodID  uint `json:"method_id" validate:"required"`
}

type Method struct {
	gorm.Model
	Method string   `db:"method" json:"method" validate:"required,lte=255"`
	Type   string   `db:"type" json:"type" validate:"required,lte=255"`
	Action []Action `gorm:"foreignKey:MethodID" validate:"required"`
}
