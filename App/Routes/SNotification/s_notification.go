package SNotification

import (
	"github.com/gofiber/fiber/v2"
	snotificationController "gitlab.com/victorrb1015/alpha_mf/App/Controller/SNotification"
)

func SetupSNotifications(router fiber.Router) {
	sn := router.Group("/s_notification")
	// Read All Notification
	sn.Get("/", snotificationController.GetSNotifications)
	//See one notification
	sn.Post("/:notificationId", snotificationController.SeenSNotification)
	//See all Notification
	sn.Post("/seen_all/:userId", snotificationController.SeenAllSNotification)
}
