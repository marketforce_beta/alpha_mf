package MethodsRoutes

import (
	"github.com/gofiber/fiber/v2"
	MethodsController "gitlab.com/victorrb1015/alpha_mf/App/Controller/Methods"
)

func SetupMethodsRoutes(router fiber.Router) {
	Method := router.Group("/method")
	//read all Methods
	Method.Get("/", MethodsController.GetMethods)
	//Create Method
	Method.Post("/", MethodsController.CreateMethod)
	//Read a Method
	Method.Get("/:methodId", MethodsController.GetMethod)
	//Update Method
	Method.Put("/:methodId", MethodsController.UpdateMethod)
	//Delete Method
	Method.Delete("/:methodId", MethodsController.DeleteMethod)
}
