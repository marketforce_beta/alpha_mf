package SUserRoutes

import (
	"github.com/gofiber/fiber/v2"
	sUserController "gitlab.com/victorrb1015/alpha_mf/App/Controller/SUser"
	"gitlab.com/victorrb1015/alpha_mf/App/middleware"
)

func SetupsUserRoutes(router fiber.Router) {
	sUser := router.Group("/suser")
	//Read all superusers
	sUser.Get("/", sUserController.GetSUsers)
	//Get count of superusers
	sUser.Get("/count", sUserController.GetCountSUser)
	//Create a new superuser
	sUser.Post("/", sUserController.CreateSUser)
	//Get one user
	sUser.Get("/:userId", middleware.JWTProtected(), sUserController.GetUser)
	//update User
	sUser.Put("/:userId", sUserController.UpdateUser)
	//delete user
	sUser.Delete("/:userId", sUserController.DeleteUser)
}
