package CompanyRoutes

import (
	"github.com/gofiber/fiber/v2"
	companyController "gitlab.com/victorrb1015/alpha_mf/App/Controller/Company"
)

func SetupCompanyRoutes(router fiber.Router){
	company := router.Group("/company")
	//Read all Company 
	company.Get("/", companyController.GetCompanies)
	//Create a Company
	company.Post("/", companyController.CreateCompanies)
	// read one Company
	company.Get("/:companyId", companyController.GetCompany)
	// Update one Company
	company.Put("/:companyId", companyController.UpdateCompany)
	// Delete one Company
	company.Delete("/:companyId", companyController.DeleteCompany)
}

