package Profile

import (
	"github.com/gofiber/fiber/v2"
	ProfileController "gitlab.com/victorrb1015/alpha_mf/App/Controller/Profile"
)

func SetupProfileRoutes(router fiber.Router) {
	Profile := router.Group("/profile")
	//Read all superusers
	Profile.Get("/:database", ProfileController.GetProfiles)
	//Create a new superuser
	Profile.Post("/:database", ProfileController.CreateProfile)
	//Get one user
	Profile.Get("/:database/:profileId", ProfileController.GetProfile)
	//update User
	Profile.Put("/:database/:profileId", ProfileController.UpdateProfile)
	//delete user
	Profile.Delete("/:database/:profileId", ProfileController.DeleteProfile)
}
