package UserRoutes

import (
	"github.com/gofiber/fiber/v2"
	UserController "gitlab.com/victorrb1015/alpha_mf/App/Controller/User"
)

func SetupUserRoutes(router fiber.Router) {
	User := router.Group("/user")
	//Read all superusers
	User.Get("/:database", UserController.GetUsers)
	//Get count of superusers
	User.Get("/:database/count", UserController.GetCountUser)
	//Create a new superuser
	User.Post("/:database", UserController.CreateUser)
	//Get one user
	User.Get("/:database/:userId", UserController.GetUser)
	//update User
	User.Put("/:database/:userId", UserController.UpdateUser)
	//delete user
	User.Delete("/:database/:userId", UserController.DeleteUser)
}
