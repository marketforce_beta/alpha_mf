package ApiRoutes

import (
	"github.com/gofiber/fiber/v2"
	ApisController "gitlab.com/victorrb1015/alpha_mf/App/Controller/Api"
)

func SetupApiRoutes(router fiber.Router) {
	api := router.Group("/api")
	//read all Apis
	api.Get("/", ApisController.GetApis)
	//Create api
	api.Post("/", ApisController.CreateApi)
	//Read a api
	api.Get("/:apiId", ApisController.GetApi)
	//Update api
	api.Put("/:apiId", ApisController.UpdateApi)
	//Delete api
	api.Delete("/:apiId", ApisController.DeleteApi)
}
