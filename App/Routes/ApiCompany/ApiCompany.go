package ApiCompany

import (
	"github.com/gofiber/fiber/v2"
	ApisCompanyController "gitlab.com/victorrb1015/alpha_mf/App/Controller/ApiCompany"
)

func SetupApiCompanyRoutes(router fiber.Router) {
	apiCompany := router.Group("/api_company")
	//Get apis
	apiCompany.Get("/:companyId", ApisCompanyController.GetApiCompanies)
	//Connect api to company
	apiCompany.Post("/", ApisCompanyController.CreateApiCompany)
	//Delete Connect
	apiCompany.Delete("/:apiId/:companyId", ApisCompanyController.DeleteApi)
}
