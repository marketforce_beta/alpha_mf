package Utils

import data "gitlab.com/victorrb1015/alpha_mf/Database"

func ConnectClient(database string) {
	db := Decrypt(database)
	data.ConnectdbClient(db)
}
