package Utils

import (
	"crypto/tls"

	"log"

	"strconv"
	"time"

	hermes "github.com/matcornic/hermes/v2"
	mail "github.com/xhit/go-simple-mail/v2"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
)

func SendEmail(body hermes.Email, to string, subject string) {
	// Configure hermes by setting a theme and your product info
	h := hermes.Hermes{
		// Optional Theme
		// Theme: new(Default)
		Product: hermes.Product{
			// Appears in header & footer of e-mails
			Name: "MarketForce",
			Link: "allamericanmbs.com",
			// Optional product logo
			Logo: "https://allamericanmbs.com/images/AABMK/Logo_MF.png",
			// Custom copyright notice
			Copyright:   "Copyright © 2022 MarketForce. All rights reserved.",
			TroubleText: "If you’re having trouble, Send Email to info@allamericanmbs.com",
		},
	}
	emailBody, erro := h.GenerateHTML(body)
	if erro != nil {
		panic(erro) // Tip: Handle error with something else than a panic ;)
	}

	port, _ := strconv.Atoi(config.Config("SMTP_PORT"))
	server := mail.NewSMTPClient()
	server.Host = config.Config("SMTP_HOST")
	server.Port = port
	server.Username = config.Config("SMTP_USERNAME")
	server.Password = config.Config("SMTP_PASS")
	server.Encryption = mail.EncryptionTLS

	smtpClient, err := server.Connect()
	if err != nil {
		log.Fatal(err)
	}
	// Create email
	email := mail.NewMSG()
	email.SetFrom("MarketForce By MBS <mf@allamericanmbs.com>")
	email.AddTo(to)
	email.SetSubject(subject)

	email.SetBody(mail.TextHTML, emailBody)

	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		log.Fatal(err)
	}

}

func WelcomeEmail() {
	email := hermes.Email{
		Body: hermes.Body{
			Name: "Jon Snow",
			Intros: []string{
				"Welcome to Marketforce! We're very excited to have you on board.",
			},
			Actions: []hermes.Action{
				{
					Instructions: "To get started with Marketforce, please click here:",
					Button: hermes.Button{
						Color: "#FF0000", // Optional action button color
						Text:  "Enter to Marketforce",
						Link:  "https://marketforceapp.com/",
					},
				},
			},
			Outros: []string{
				"Need help, or have questions? Just reply to this email, we'd love to help.",
			},
		},
	}
	SendEmail(email, "victor@grupocomunicado.com", "Welcome to Marketforce")
}

func TestEmail() {
	const htmlBody = `<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Hello Gophers!</title>
	</head>
	<body>
		<p>This is the <b>Go gopher</b>.</p>
		<p>Image created by Francisco Gil</p>
	</body>
	</html>`

	server := mail.NewSMTPClient()

	// SMTP Server
	server.Host = "mtp.mailgun.org"
	server.Port = 587
	server.Username = "postmaster@allamericanmbs.com"
	server.Password = "7dd4c3e62737c0deba28dc8cc720f2fd-835621cf-7e16272d"
	// server.Encryption = mail.EncryptionSTARTTLS

	// Since v2.3.0 you can specified authentication type:
	// - PLAIN (default)
	// - LOGIN
	// - CRAM-MD5
	// server.Authentication = mail.AuthPlain

	// Variable to keep alive connection
	server.KeepAlive = false

	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 10 * time.Second

	// Timeout for send the data and wait respond
	server.SendTimeout = 10 * time.Second

	// Set TLSConfig to provide custom TLS configuration. For example,
	// to skip TLS verification (useful for testing):
	server.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         server.Host,
	}

	// SMTP client
	smtpClient, err := server.Connect()

	if err != nil {
		log.Fatal(err)
	}

	// New email simple html with inline and CC
	email := mail.NewMSG()
	email.SetFrom("postmaster@mf.allamericanmbs.com").
		AddTo("francisco@grupocomunicado.com").
		AddTo("victor@grupocomunicado.com").
		SetSubject("New Go Email Test")

	email.SetBody(mail.TextHTML, htmlBody)
	email.AddAlternative(mail.TextPlain, "Hello Gophers!")

	// always check error after send
	if email.Error != nil {
		log.Println("Expected nil, got", email.Error, "generating email")
	}

	// Call Send and pass the client
	err = email.Send(smtpClient)

	//Get first error
	email.GetError()

	if err != nil {
		log.Println(err)
	} else {
		log.Println("Email Sent")
	}

}
