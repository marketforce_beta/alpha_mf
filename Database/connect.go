package database

import (
	"errors"
	"fmt"
	"log"
	"strconv"

	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	seed "gitlab.com/victorrb1015/alpha_mf/Database/Seeds"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// DB Declare the variable for the database
var DB *gorm.DB

// ConnectDB connect to db
func ConnectDB() {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	if err != nil {
		log.Println("Idiot")
	}

	// Connection URL to connect to Postgres CompanyDb
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), port, config.Config("DB_NAME"))
	// Connect to the DB and initialize the DB variable
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Println("failed to connect database : " + dsn)
	}

	fmt.Println("Connection Opened to CompanyDb")
	// Migrate the database
	if err = DB.AutoMigrate(&model.Company{}, &model.SUser{}, &model.SNotification{}, &model.Api{}, &model.ApiCompany{}, &model.Methods{}); err == nil && DB.Migrator().HasTable(&model.SUser{}) {
		if err := DB.First(&model.SUser{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
			//Insert seed data
			err = DB.Debug().Model(&model.Company{}).Create(&seed.Company).Error
			if err != nil {
				log.Fatalf("cannot seed company table: %v", err)
			}
			for i, _ := range seed.Users {
				err = DB.Debug().Model(&model.SUser{}).Create(&seed.Users[i]).Error
				if err != nil {
					log.Fatalf("cannot seed users table: %v", err)
				}
			}
		}
	}
	fmt.Println("CompanyDb Migrated")
}
