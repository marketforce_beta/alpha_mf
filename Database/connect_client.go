package database

import (
	"errors"
	"fmt"
	seed "gitlab.com/victorrb1015/alpha_mf/Database/Seeds"
	"log"
	"strconv"

	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DbC *gorm.DB

func ConnectdbClient(DbName string) {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	if err != nil {
		log.Println("Idiot")
	}

	// Connection URL to connect to Client CompanyDb
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), port, DbName)

	DbC, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Println("failed to connect database client")
	}

	//fmt.Println("Connection Opened to CompanyDb")
	if err = DbC.AutoMigrate(&model.Profile{}, &model.User{}, &model.Notification{}, &model.Method{}, &model.Action{}); err == nil && DbC.Migrator().HasTable(&model.Profile{}) {
		if err := DbC.First(&model.Profile{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
			//Insert seed data
			err = DbC.Debug().Model(&model.Profile{}).Create(&seed.ProfileClient).Error
			if err != nil {
				log.Fatalf("cannot seed company table: %v", err)
			}
		}
	}
	if err != nil {
		log.Println("failed to Migrated database client")
	}
	fmt.Println(" CompanyDb " + DbName + " Migrated ")
}
