package Seeds

import (
	model "gitlab.com/victorrb1015/alpha_mf/App/Model"
)

var Users = []model.SUser{
	{
		CompanyId: 1,
		Name:      "Victor",
		LastName:  "Rojas",
		Username:  "vrojas",
		Password:  "$2a$14$Y1DRY7Qt5AaNgKTlT.l/Q.P.0JxsgXB5MXSk8BvDrCa9zf6APwnxG",
		Email:     "victor@grupocomunicado.com",
	},
	model.SUser{
		CompanyId: 1,
		Name:      "Francisco",
		LastName:  "Gil",
		Username:  "Francisco",
		Password:  "$2a$14$thuEYDQ3MQEoawFfMxQKye5eXdSXQaKF430IUQHSt72uiq8XFBjiO",
		Email:     "francisco@grupocomunicado.com",
	},
	model.SUser{
		CompanyId: 1,
		Name:      "Gustavo",
		LastName:  "De La Rosa",
		Username:  "gtavo",
		Password:  "$2a$14$S/jxLf6Opi58B6c6ncRiU.s3K7brPXCHCkaHYoM.Ww1rp82wrazIm",
		Email:     "gustavo@grupocomunicado.com",
	},
}

var Company = model.Company{
	FullName:     "Grupo Comunicado 7",
	Address:      "This is a test address 7",
	Abbreviation: "grupo",
	Logo:         "https://ui-avatars.com/api/?name=Grupo+Comunicado",
	OrdersLogo:   "https://ui-avatars.com/api/?name=Grupo+Comunicado",
	Country:      "Mexico",
	City:         "Queretaro",
	State:        "Queretaro",
	ZipCode:      "76146",
	EmailFrom:    "victor@grupocomunicado.com",
	Phone:        "55555555",
	CompanyDb:    "5e9becbe1d532b8ed3dc71ae75b7510110fd81ef5d243a902fc5877613b164292a1da3f54aa3fd2431df17eafd22f0ebeb",
	WebSite:      "grupocomunicado.com",
}

var ProfileClient = []model.Profile{
	{
		Name:        "Administrator",
		Description: "Has full control of the system",
	},
	model.Profile{
		Name:        "Manager",
		Description: "Has access to some special features",
	},
	model.Profile{
		Name:        "General",
		Description: "Has limited permissions",
	},
	
}
