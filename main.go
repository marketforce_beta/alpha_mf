package main

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"

	"github.com/gofiber/fiber/v2"
	utils "gitlab.com/victorrb1015/alpha_mf/App/Utils"
	"gitlab.com/victorrb1015/alpha_mf/App/middleware"
	config "gitlab.com/victorrb1015/alpha_mf/Config"
	router "gitlab.com/victorrb1015/alpha_mf/Router"
)

func main() {
	// Start a new fiber app
	app := fiber.New()
	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Connect to the CompanyDb

	// Send a string back for GET calls to the endpoint "/"
	app.Get("/", func(c *fiber.Ctx) error {
		err := c.SendString("And the API is UP!")
		return err
	})

	app.Get("/api/email", func(c *fiber.Ctx) error {
		utils.WelcomeEmail()
		err := c.SendString("Email Send!")
		return err
	})

	app.Post("/upload", func(c *fiber.Ctx) error {
		f, err := c.FormFile("file")
		if err != nil {
			return err
		}
		file, _ := f.Open()
		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		part, _ := writer.CreateFormFile("file", f.Filename)
		_, _ = io.Copy(part, file)
		_ = writer.Close()
		url := "https://storage.googleapis.com/upload/storage/v1/b/" + config.Config("BUCKET_NAME") + "/o?uploadType=media&name=" + f.Filename
		r, _ := http.NewRequest("POST", url, body)
		r.Header.Set("Content-Type", writer.FormDataContentType())
		authorization := "Bearer " + config.Config("GOOGLE_CLOUD_API_KEY")
		r.Header.Set("Authorization", authorization)
		client := &http.Client{}
		resp, err := client.Do(r)
		if err != nil {
			return err
		}
		return c.SendStatus(resp.StatusCode)
	})

	router.PublicRoutes(app)
	router.PrivateRoutes(app)
	//router.NotFoundRoute(app)

	// Start server (with graceful shutdown).
	utils.StartServer(app)
}
