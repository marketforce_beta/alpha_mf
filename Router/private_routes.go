package router

import (
	"github.com/gofiber/fiber/v2"
	apiroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/Api"
	apicompanyroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/ApiCompany"
	companyRoutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/Company"
	MethodsRoutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/Methods"
	profileroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/Profile"
	snotificationroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/SNotification"
	suserroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/SUser"
	userroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/User"
)

func PrivateRoutes(app *fiber.App) {
	api := app.Group("/api")
	companyRoutes.SetupCompanyRoutes(api)
	userroutes.SetupUserRoutes(api)
	suserroutes.SetupsUserRoutes(api)
	snotificationroutes.SetupSNotifications(api)
	apiroutes.SetupApiRoutes(api)
	MethodsRoutes.SetupMethodsRoutes(api)
	apicompanyroutes.SetupApiCompanyRoutes(api)
	profileroutes.SetupProfileRoutes(api)
}
