package router

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	authcontroller "gitlab.com/victorrb1015/alpha_mf/App/Controller/Auth"
	companyRoutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/Company"
	suserroutes "gitlab.com/victorrb1015/alpha_mf/App/Routes/SUser"
)

func SetRoutes(app *fiber.App) {
	api := app.Group("/api", logger.New())
	api.Post("/login", authcontroller.Login)
	companyRoutes.SetupCompanyRoutes(api)
	suserroutes.SetupsUserRoutes(api)
}
