package router

import (
	"github.com/gofiber/fiber/v2"
	authcontroller "gitlab.com/victorrb1015/alpha_mf/App/Controller/Auth"
)

func PublicRoutes(app *fiber.App) {
	api := app.Group("/api")
	api.Post("/login", authcontroller.Login)
}
